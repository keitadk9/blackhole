const express = require("express");
const volleyball = require('volleyball');
const fs = require('fs');
const arrayBufferToBuffer = require('arraybuffer-to-buffer');
const pako = require('pako');

const MAX_USER = 1000;


app = express();
app.use(volleyball);
app.use(express.json(({limit: '50mb'})));
const userConnected = new Map();




server = app.listen(process.env.PORT || 3000, () => {
    console.log(`Server is running on port ${process.env.PORT || 3000}`);
});


function stringToBinaryArr(str) {
    var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
    var bufView = new Uint16Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}



const io = require('socket.io')(

    server, {
        pingInterval: 50000,
        pingTimeout: 5000,
        cookie: false
    });

// io.set('transports', ['websocket']);

io.sockets.on('connection', client => {
    clientInfo = client.handshake.query;

    // if (clientInfo.session_ID != "") {
    //     client.myID = clientInfo.session_ID;
    //     userConnected.set(client.session_ID, client);

    //     console.log("SESSION_RECUPERATION at connection from " + clientInfo.username + " id: " + client.myID);
    // } else {

    //     client.emit("SESSION_ID", client.id);
    // }

    userConnected.set(client.id, client);

    console.log("[] => Got new Client (" + clientInfo.username + ") ; id " + client.id);
    // console.log(io)

    // console.log("New User list");
    // for (let [k, v] of userConnected) {
    //     const user = v;
    //     const userInfo = user.handshake.query;
    //     console.log(userInfo.username + " / id : " + k);

    // }

    // console.log(clientInfo);
    //We notify others users of an new connection
    client.broadcast.emit("newConnection", { username: clientInfo.username, groupe: clientInfo.groupe, id: client.id });

    //We also send back to new  user all  user already Connected
    for (let [k, v] of userConnected) {
        const user = v;

        if (user.id != client.id) {
            const friendInfo = user.handshake.query;
            client.emit("newConnection", { username: friendInfo.username, groupe: friendInfo.groupe, id: user.id });

        }
    }


    client.on("error",(err) => {
        if(err){
            console.log(err);
        }
    });
    client.on('disconnect', (reason) => {
        clientInfo = client.handshake.query;
        console.log("[] <= Lost a client reason (" + clientInfo.username + ") : " + reason);
        userConnected.delete(client.id);
        console.log("User => " + userConnected.size);
        client.broadcast.emit("lostConnection", { username: clientInfo.username, groupe: clientInfo.groupe, id: client.id });
    });

    client.on('tchat', (data) => {
        console.log("[] New Message");
        console.log(data);
    })

    client.on('files', (data) => {
        
        console.log("[] Got a new file");
        var output = pako.inflate(data,{to : 'string'});
        console.log(typeof output);


        const files = output.split(';');
        files.pop();


        const filesToSendForth = [];
        // console.log(files);


        (files).forEach(fileString => {
            file = JSON.parse(fileString);
            // console.log(data);


            const idKey = client.myID ? client.myID : client.id;

            const sender = userConnected.get(idKey);
            senderInfo = client.handshake.query;

            // console.log(file);
            const destinations = file.friendssocketId;
            // console.log(destinations);

            console.log("| name : " + file.fileName + " From : " + senderInfo.username);


            filesToSendForth.push(file);

            const binaryFile = Buffer.from(file.data, 'utf8');
            // console.log(binaryFile);
            // // console.log(binaryFile);

            // if (!fs.existsSync('./files')) {
            //     fs.mkdirSync('./files');
            // }

            // fs.writeFile(`./files/${file.fileName}`, binaryFile, (err) => {
            //     if (err) {
            //         console.log(err);
            //     } else {
            //         console.log("File saved");
            //     }
            // });

        });

        destinations = filesToSendForth[0].friendssocketId;

        destinations.forEach(soketId => {
            friendSocket = userConnected.get(soketId);
            // console.log(friendSocket);   

            if (friendSocket != null) {
                console.log("Emetting...");
                friendSocket.binary(true).emit("uncomingFile", filesToSendForth);
            } else {
                console.log("Socket not found");
            }
        });



    });

    client.on('file', (data) => {

        console.log("[] Got a new file");

        const file = JSON.parse(data);



        const idKey = client.myID ? client.myID : client.id;

        const sender = userConnected.get(idKey);
        senderInfo = client.handshake.query;

        console.log(file);
        const destinations = file.friendssocketId;
        // console.log(destinations);



        destinations.forEach(soketId => {
            friendSocket = userConnected.get(soketId);
            if (friendSocket) {
                friendSocket.emit("uncomingFile", filesToSendForth);
            }
        });


        console.log("| name : " + file.fileName + " From : " + senderInfo.username);



        const binaryFile = Buffer.from(file.data, 'utf8');
        // console.log(binaryFile);
        // // console.log(binaryFile);

        if (!fs.existsSync('./files')) {
            fs.mkdirSync('./files');
        }

        fs.writeFile(`./files/${file.fileName}`, binaryFile, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log("File saved");
            }
        });
    });
});


app.post('/files',(request,response) => {

    console.log(request);
})