﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackHole
{
    class NewFriendConnectedArgs : EventArgs
    {
        private string username;
        private string groupe;
        private String id;

        public NewFriendConnectedArgs(string username, string groupe,string id)
        {
            this.username = username;
            this.groupe = groupe;
            this.id = id;
        }

        public String getUsername()
        {
            return username;
        }

        public String getGroupe()
        {
            return groupe;
        }

        public String getId()
        {
            return id;
        }
    }
}
