﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackHole
{
    class Friend
    {
        private String name;
        private String group;
        private String id;

        public Friend(String n,String g, String i)
        {
            this.name = n;
            this.group = g;
            this.id = i;
        }

        public String getName()
        {
            return name;
        }

        public String getGroupe()
        {
            return group;
        }
        public String getId()
        {
            return id;
        }


    }
}
