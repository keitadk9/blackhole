﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackHole
{
    [Serializable]
    class Data
    {
        public String fileName;
        public List<byte> data;
        public List<String> friendssocketId;
        public String extra;

        public Data(String name,List<String> FriendsocketId, byte[] data,String extra)
        {
            this.fileName = name;
            this.friendssocketId = FriendsocketId;
            this.data = new List<byte>();
            this.extra = extra;

            foreach(byte b in data)
            {
                this.data.Add(b);
                
            }
        }

        public Data(String name, String FriendsocketId, byte[] data,String extra)
        {
            this.fileName = name;
            this.friendssocketId = new List<string> { FriendsocketId };
            this.data = new List<byte>();
            this.extra = extra;

            foreach (byte b in data)
            {
                this.data.Add(b);

            }
        }

    }
}
