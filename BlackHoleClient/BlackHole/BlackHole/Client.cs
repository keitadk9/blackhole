﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;
using Newtonsoft;
using System.Web;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Threading;
using Quobject.Collections.Immutable;
using System.IO.Compression;

namespace BlackHole
{
    class Client
    {

        public static int MAX_FILE_ONE = 3;
        public static int MAX_FILE_LENGHT = 66431277;
        private Socket socket = null;
        private String username;
        private String serverAdress;
        private String groupe;
        private int serverPort;
        private bool connected;
        private bool hasPort ;




        public event EventHandler newFriendEvent;
        public event EventHandler lostFriendConnectionEvent;
        public event EventHandler disconnectionEvent;

        public event EventHandler fileToSendPack;





        public Client(String serverAdress, int serverPort, String username, String groupe)
        {
            this.username = username;
            this.serverAdress = serverAdress;
            this.serverPort = serverPort;
            this.groupe = groupe;
            this.connected = false;
            this.hasPort = true ;
        }

        public Client(String serverAdress, String username, String groupe)
        {
            this.username = username;
            this.serverAdress = serverAdress;
            this.groupe = groupe;
            this.connected = false;
            this.hasPort = false;
        }

        public void connect()
        {
            IO.Options opts = new IO.Options();
            opts.Secure = true;
            opts.Transports = ImmutableList.Create<string>("websocket");
            Dictionary<String, String> query = new Dictionary<String, String>();
            query.Add("username", this.username);
            query.Add("groupe", this.groupe);
            //query.Add("session_ID", Session_ID);

            opts.Query = query;

            if (hasPort)
            {
                socket = IO.Socket(serverAdress + ":" + serverPort, opts);
            }
            else
            {
                socket = IO.Socket(serverAdress, opts);
            }


            socket.On(Socket.EVENT_CONNECT, () =>
            {
                //MessageBox.Show("Connected");
                this.connected = true;
                //if (Session_ID != "")
                //{
                //    socket.Emit("session_recup", Session_ID);
                //}
            });



       

            socket.On(Socket.EVENT_DISCONNECT, (err) =>
            {
                //MessageBox.Show(err.ToString());
                this.connected = false;
                socket.Close();
                EventHandler handler = disconnectionEvent;
                handler(this, EventArgs.Empty);




            });

            socket.On(Socket.EVENT_RECONNECT, () =>
            {

            }); 

            socket.On("error", (err) =>
            {
                MessageBox.Show(err.ToString());
                socket.Close();
                EventHandler handler = disconnectionEvent;
                handler(this, EventArgs.Empty);
            });

            socket.On("newConnection", (data) =>
            {
                //MessageBox.Show("New connection");
                Newtonsoft.Json.Linq.JObject friendData = (Newtonsoft.Json.Linq.JObject)data;

                Newtonsoft.Json.Linq.JToken friendName = friendData.GetValue("username");
                Newtonsoft.Json.Linq.JToken friendGroup = friendData.GetValue("groupe");
                Newtonsoft.Json.Linq.JToken friendId = friendData.GetValue("id");

                EventHandler handler = newFriendEvent;
                handler(this, new NewFriendConnectedArgs(friendName.ToString(), friendGroup.ToString(), friendId.ToString()));
            });

            socket.On("lostConnection", (data) =>
            {
                //MessageBox.Show("New connection");
                Newtonsoft.Json.Linq.JObject friendData = (Newtonsoft.Json.Linq.JObject)data;

                Newtonsoft.Json.Linq.JToken friendName = friendData.GetValue("username");
                Newtonsoft.Json.Linq.JToken friendGroup = friendData.GetValue("groupe");
                Newtonsoft.Json.Linq.JToken friendId = friendData.GetValue("id");

                EventHandler handler = lostFriendConnectionEvent;
                handler(this, new NewFriendConnectedArgs(friendName.ToString(), friendGroup.ToString(), friendId.ToString()));
            });

            socket.On("uncomingFile", (data) =>
            {
                new Thread(delegate ()
                {
                    Newtonsoft.Json.Linq.JArray filesList = ((Newtonsoft.Json.Linq.JArray)data);

                    foreach (Newtonsoft.Json.Linq.JObject file in filesList)
                    {
                        Newtonsoft.Json.Linq.JToken fileName = file.GetValue("fileName");
                        List<Newtonsoft.Json.Linq.JToken> fileData = file.GetValue("data").ToList();
                        byte[] fileBuffer = new byte[fileData.Count];
                        int index = 0;

                        foreach (byte b in fileData)
                        {
                            fileBuffer[index] = b;
                            index++;
                        }

                        File.WriteAllBytes("./Recevied/" + fileName.ToString(), fileBuffer);

                    }


                    //MessageBox.Show(data.ToString());
                }).Start();



            });



        }

        public void sendFile(String path, String name, List<String> friendsocketId, String extra)
        {
            String filePath = path;
            byte[] buffer = System.IO.File.ReadAllBytes(filePath);




            Data data = new Data(name, friendsocketId, buffer, extra);




            //MessageBox.Show(data.data);
            String jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);

            socket.Emit("file", jsonData);
        }


        public void sendFiles(List<String> filesPath, List<String> friendsocketId)
        {

            StringBuilder finalData = new StringBuilder();
            for (int i = 0; i < filesPath.Count; i++)
            {
                String path = filesPath.ElementAt(i);
                byte[] buffer = System.IO.File.ReadAllBytes(path);

                String[] pathComponent = path.Split('\\');
                String name = pathComponent[pathComponent.Length - 1];

                Data data = new Data(name, friendsocketId, buffer, "");

                String jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
                
                finalData.Append(jsonData + ";");

                EventHandler handler = fileToSendPack;
                handler(this, EventArgs.Empty);

                //MessageBox.Show("Size : " + finalData.ToString().Length + "");

                if (i == filesPath.Count - 1 || (i + 1) % MAX_FILE_ONE == 0)
                {

                    byte[] compressed = Zip(finalData.ToString());

                    socket.Emit("files",compressed);
                    finalData.Clear();
                }


            }

        }



        public bool isConnected()
        {
            return this.connected;
        }

        public static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }

        public static byte[] Zip(string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);

            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    //msi.CopyTo(gs);
                    CopyTo(msi, gs);
                }

                return mso.ToArray();
            }
        }

        public static string Unzip(byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    //gs.CopyTo(mso);
                    CopyTo(gs, mso);
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }

        



    }
}
