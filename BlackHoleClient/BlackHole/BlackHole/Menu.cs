﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Quobject.SocketIoClientDotNet.Client;


namespace BlackHole
{
    public partial class Menu : Form
    {
        private delegate void SafeFriendConnectedCall(object sender, EventArgs e);
        private Client client;
        private List<Friend> friendConnected;
        private double fileTransfertProgress;
        private double stepTransfertProgress;


       



        public Menu()
        {
            InitializeComponent();

            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            name.Focus();
            friendConnected = new List<Friend>();
        }

        private void listeFichier_DoubleClick(object sender,EventArgs e)
        {
            if(MessageBox.Show("Supprimer le fichier de la liste ? ","",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                listeFichier.Items.RemoveAt(listeFichier.SelectedIndex);
            }
        }
        


        
        private void connectBtn_Click(object sender, EventArgs e)
        {
            

            if (client == null || !client.isConnected())
            {
                //client = new Client(serverAdress.Text, Int32.Parse(serverPort.Text), username.Text);

                if (name.Text != "" && groupe.Text != "")
                {

                    client = new Client("http://localhost", 3000, name.Text, groupe.Text);
                    client.connect();
                    connectBtn.Text = "Déconnexion";
                    client.newFriendEvent += Client_newFriendEvent;
                    client.lostFriendConnectionEvent += Client_lostFriendConnectionEvent;
                    client.fileToSendPack += Client_fileToSendPack;
                    client.disconnectionEvent += Client_disconnectionEvent;

                }
                else
                {
                    MessageBox.Show("Veuillez saisir un " + (name.Text == "" ? "nom" : "groupe"));
                    if (name.Text == "")
                    {
                        username.Focus();
                    }
                    else
                    {
                        groupe.Focus();
                    }
                }
            }

        }

        private void Client_disconnectionEvent(object sender, EventArgs e)
        {
            if (connectListBox.InvokeRequired)
            {
                var d = new SafeFriendConnectedCall(Client_disconnectionEvent);
                connectListBox.Invoke(d, new object[] { sender, e });

            }
            else
            {
                connectListBox.Items.Clear();
                friendConnected.Clear();
            }
        }

        private void Client_fileToSendPack(object sender, EventArgs e)
        {
            if (transfertProgress.InvokeRequired)
            {
                var d = new SafeFriendConnectedCall(Client_fileToSendPack);
                transfertProgress.Invoke(d, new object[] { sender, e });

            }
            else
            {
                fileTransfertProgress += stepTransfertProgress;
                transfertProgress.Value = (int)fileTransfertProgress;
                
            }
        }

            private void Client_lostFriendConnectionEvent(object sender, EventArgs e)
        {
            NewFriendConnectedArgs args = (NewFriendConnectedArgs)e;

            if (connectListBox.InvokeRequired)
            {
                var d = new SafeFriendConnectedCall(Client_lostFriendConnectionEvent);
                connectListBox.Invoke(d, new object[] { sender, e });


            }
            else
            {
                connectListBox.Items.RemoveAt(connectListBox.Items.IndexOf(args.getUsername() + " / grp : " + args.getGroupe()));
                for(int i = 0; i< friendConnected.Count; i++)
                {
                    Friend f = friendConnected.ElementAt(i);
                    if(f.getId() == args.getId())
                    {
                        friendConnected.RemoveAt(i);
                        break;
                    }
                    
                }
            }
            
        }

        private void Client_newFriendEvent(object sender, EventArgs e)
        {
            NewFriendConnectedArgs args = (NewFriendConnectedArgs) e;

            if (connectListBox.InvokeRequired)
            {
                var d = new SafeFriendConnectedCall(Client_newFriendEvent);
                connectListBox.Invoke(d, new object[] { sender, e });


            }
            else
            {
                connectListBox.Items.Add(args.getUsername() + " / grp : " + args.getGroupe());
                friendConnected.Add(new Friend(args.getUsername(), args.getGroupe(), args.getId()));
            }
            
        }

        private void fileBtn_Click(object sender, EventArgs e)
        {
            if (fileManager.ShowDialog() == DialogResult.OK)
            {
                fileListeBox.Items.AddRange(fileManager.FileNames);
            }

        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Vider la liste des fichiers chargé ?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                fileListeBox.Items.Clear();
            }
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {

            if ( client == null || !client.isConnected())
            {
                MessageBox.Show("Veuillez vous connecter");
                return;
            }

            if (connectListBox.Items.Count == 0)
            {

                MessageBox.Show("Aucun amis n'est connecté actuellement");
                return;
            }


            if (connectListBox.SelectedIndex == -1)
            {
                MessageBox.Show("Veuillez selectionner au moins un amis");
                return;
            }

            if (fileListeBox.Items.Count == 0)
            {
                MessageBox.Show("Veuillez selectionner au moins un fichier");
                return;
            }

            ListBox.SelectedIndexCollection friendSelected = connectListBox.SelectedIndices;
            List<String> friendSocketIds = new List<String>();
            List<String> filesPath = new List<string>();

            foreach (int index in friendSelected)
            {
                friendSocketIds.Add(friendConnected.ElementAt(index).getId());
            }

            foreach(String path in fileListeBox.Items)
            {
                filesPath.Add(path);
            }



            stepTransfertProgress = (double)100 / fileListeBox.Items.Count;
            fileTransfertProgress = 0;


            client.sendFiles(filesPath, friendSocketIds);

            //foreach (String path in fileListeBox.Items)
            //{

            //    String[] pathComponent = path.Split('\\');
            //    String name = pathComponent[pathComponent.Length - 1];

            //    client.sendFile(path, name, friendSocketIds,"");
            //    //MessageBox.Show("File : " + name + " sended");
            //    //total += d;
            //    //transfertProgress.Value = (int)total;

            //}


            MessageBox.Show("Transfert terminer");
            fileListeBox.Items.Clear();
            transfertProgress.Value = 0;
        }

        private void fileListe_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        private void fileListe_DragDrop(object sender, DragEventArgs e)
        {
            string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            int i;
            for (i = 0; i < s.Length; i++)
                fileListeBox.Items.Add(s[i]);
        }

     
    }
}
